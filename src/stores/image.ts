import { ref } from "vue";
import { defineStore } from "pinia";
import type image from "@/type/image";
export const useImageStore = defineStore("image", () => {
  const images = ref<image[]>([
    { id: 1, name: "ANGRY", img: "../public/angry.png" },
    { id: 2, name: "CUTE", img: "../public/cute.png" },
    { id: 3, name: "EXCITED", img: "../public/excited.png" },
    { id: 4, name: "PAIN", img: "../public/pain.png" },
    { id: 5, name: "SHOCKED", img: "../public/shocked.png" },
    { id: 6, name: "SMILE", img: "../public/smile.png" },
  ]);
  const Emote = (id: number): void => {
    const index = images.value.findIndex((item) => item.id === id);
    images.value[index].name;
  };
  return { images, Emote };
});

export default interface image {
  id: number;
  name: string;
  img: string;
}
